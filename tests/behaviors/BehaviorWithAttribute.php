<?php
namespace alexs\yii2tubularload\tests\behaviors;
use yii\base\Behavior;
use yii\helpers\Html;
use yii\db\ActiveRecord;

class BehaviorWithAttribute extends Behavior
{
    public $attribute;
    public $real_attribute;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this->real_attribute = Html::getAttributeName($this->attribute);

    }

    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE=>'beforeValidate',
        ];
    }

    public function beforeValidate() {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        // we have an access to the specific attribute '[0]image' in the $_POST
        // for example upload like
        // yii\web\UploadedFile::getInstance($model, $this->attribute);
    }
}