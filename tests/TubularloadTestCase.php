<?php
namespace alexs\yii2tubularload\tests;
use alexs\yii2phpunittestcase\DatabaseTestCase;

class TubularloadTestCase extends DatabaseTestCase
{
    protected function setUp():void {
        parent::setUp();
        \Yii::$app->db->createCommand()->setSql("
CREATE TABLE country(id INTEGER PRIMARY KEY, name TEXT, population INT);
CREATE TABLE city (id INTEGER PRIMARY KEY, country_id INT, name TEXT, is_capital INT, foundation INT, image TEXT);
 ")->execute();
    }

    protected function tearDown():void {
        \Yii::$app->db->createCommand()->setSql("
DROP TABLE `city`;
DROP TABLE `country`
        ")->execute();
        parent::tearDown();
    }
}