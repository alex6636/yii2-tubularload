<?php
namespace alexs\yii2tubularload\tests;
use alexs\yii2tubularload\tests\behaviors\BehaviorWithAttribute;
use alexs\yii2tubularload\tests\models\Country;
use alexs\yii2tubularload\tests\models\City;

class TubularloadTest extends TubularloadTestCase
{
    public function testCreate() {
        $data = [
            'Country'=>[
                'name'=>'USA',
                'population'=>328200000,
            ],
            'City'=>[
                [
                    'name'=>'Washington',
                    'is_capital'=>1,
                    'foundation'=>1790,
                ],
                [
                    'name'=>'New York',
                    'is_capital'=>0,
                    'foundation'=>1624,
                ],
                [
                    'name'=>'Miami',
                    'is_capital'=>0,
                    'foundation'=>1896,
                ],
            ],
        ];
        \Yii::$app->request->setBodyParams($data);
        $country = new Country();
        $country->load(\Yii::$app->request->post());
        $cities = City::tubularloadCreate('\alexs\yii2tubularload\tests\models\City');
        $valid = $country->validate() && City::validateMultiple($cities);
        // test validation
        $this->assertTrue($valid);
        $country->save(false);
        City::tubularloadSave($cities, 'country', $country);
        $cities = City::find()->all();
        // check each city
        foreach ($cities as $i=>$city) {
            $this->assertSame($country->id, $city->country_id);
            $this->assertSame($data['City'][$i]['name'], $city->name);
            $this->assertSame($data['City'][$i]['is_capital'], $city->is_capital);
            $this->assertSame($data['City'][$i]['foundation'], $city->foundation);
        }
    }

    public function testUpdate() {
        $country = new Country([
            'id'=>1,
            'name'=>'USA',
            'population'=>328200000,
        ]);
        $country->save();
        $city = new City([
            'id'=>1,
            'name'=>'Washington',
            'country_id'=>$country->id,
            'is_capital'=>1,
            'foundation'=>1790,
        ]);
        $city->save();
        $city = new City([
            'id'=>2,
            'name'=>'New York',
            'country_id'=>$country->id,
            'is_capital'=>0,
            'foundation'=>1624,
        ]);
        $city->save();
        $data = [
            'City'=>[
                [
                    'id'=>1,
                    'name'=>'Washington, D.C.',
                    'is_capital'=>1,
                    'foundation'=>1790,
                ],
                [
                    'id'=>2,
                    'name'=>'NY',
                    'is_capital'=>0,
                    'foundation'=>1624,
                ],
                [
                    'name'=>'Miami',
                    'is_capital'=>0,
                    'foundation'=>1896,
                ],
            ],
        ];
        \Yii::$app->request->setBodyParams($data);
        $cities = City::tubularloadUpdate('\alexs\yii2tubularload\tests\models\City', ['country_id'=>$country->id]);
        $valid = City::validateMultiple($cities);
        $this->assertTrue($valid);
        City::tubularloadSave($cities, 'country', $country);
        $this->assertSame('Washington, D.C.', $cities[0]->name);
        $this->assertSame('NY', $cities[1]->name);
        $this->assertSame(count($cities), 3);
        $this->assertSame($cities[2]->getAttributes(), [
            'id'=>3,
            'country_id'=>$country->id,
            'name'=>'Miami',
            'is_capital'=>0,
            'foundation'=>'1896',
            'image'=>null,
        ]);
    }

    public function testFilterEnabled() {
        $data = [
            'Country'=>[
                'name'=>'USA',
                'population'=>328200000,
            ],
            'City'=>[
                [
                    'name'=>'Washington',
                    'is_capital'=>1,
                    'foundation'=>1790,
                ],
                [
                    'name'=>'',
                    'is_capital'=>'',
                    'foundation'=>'',
                ],
                [
                    'name'=>'Miami',
                    'is_capital'=>0,
                    'foundation'=>1896,
                ],
            ],
        ];
        \Yii::$app->request->setBodyParams($data);
        $cities = City::tubularloadCreate('\alexs\yii2tubularload\tests\models\City');
        $valid = City::validateMultiple($cities);
        $this->assertTrue($valid);
        $this->assertSame(count($cities), 2);
    }

    public function testFilterDisabled() {
        $data = [
            'Country'=>[
                'name'=>'USA',
                'population'=>328200000,
            ],
            'City'=>[
                [
                    'name'=>'Washington',
                    'is_capital'=>1,
                    'foundation'=>1790,
                ],
                [
                    'name'=>'',
                    'is_capital'=>'',
                    'foundation'=>'',
                ],
                [
                    'name'=>'Miami',
                    'is_capital'=>0,
                    'foundation'=>1896,
                ],
            ],
        ];
        \Yii::$app->request->setBodyParams($data);
        City::$tubularload_filter_values = false;
        $cities = City::tubularloadCreate('\alexs\yii2tubularload\tests\models\City');
        $valid = City::validateMultiple($cities);
        $this->assertFalse($valid);
        $this->assertSame(count($cities), 3);
    }

    public function testAttachBehaviors() {
        $data = [
            'Country'=>[
                'name'=>'USA',
                'population'=>328200000,
            ],
            'City'=>[
                [
                    'name'=>'Washington',
                    'is_capital'=>1,
                    'foundation'=>1790,
                    'image'=>'image_first.jpg',
                ],
                [
                    'name'=>'New York',
                    'is_capital'=>0,
                    'foundation'=>1624,
                    'image'=>'image_second.jpg',
                ],
                [
                    'name'=>'Miami',
                    'is_capital'=>0,
                    'foundation'=>1896,
                    'image'=>'image_third.jpg',
                ],
            ],
        ];
        $behaviors = [
            'BehaviorWithAttribute'=>[
                [
                    'class'=>BehaviorWithAttribute::class,
                ],
                ['attribute'=>'image'],
            ],
        ];
        \Yii::$app->request->setBodyParams($data);
        $cities = City::tubularloadCreate('\alexs\yii2tubularload\tests\models\City', $behaviors);
        City::validateMultiple($cities);
        $this->assertSame('[0]image', $cities[0]->getBehavior('BehaviorWithAttribute')->attribute);
        $this->assertSame('[1]image', $cities[1]->getBehavior('BehaviorWithAttribute')->attribute);
        $this->assertSame('[2]image', $cities[2]->getBehavior('BehaviorWithAttribute')->attribute);
    }
}