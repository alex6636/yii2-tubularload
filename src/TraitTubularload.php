<?php
namespace alexs\yii2tubularload;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;

trait TraitTubularload
{
    /**
     * Drop rows that have only empty values
     * @var bool $tubularload_filter_values
     */
    public static $tubularload_filter_values = true;
    public static $tubularload_filtered_values = [[], '', null];
    
    /**
     * The primary key
     * @var string $tubularload_pk_attr
     */
    public static $tubularload_pk_attr = 'id';

    /**
     * Loading models when we create them
     *
     * @param string $model_name
     * for example: 'app\models\City'
     * @param array $behaviors behaviors which have dynamic attributes
     * @return ActiveRecord[]
     */
    public static function tubularloadCreate($model_name, array $behaviors = []) {
        $models = [];
        $data = static::tubularloadGetData($model_name);
        if (static::$tubularload_filter_values) {
            $data = array_filter(array_map(function($_data) {
                return array_filter($_data, ['\alexs\yii2tubularload\TraitTubularload', 'tubularloadFilter']);
            }, $data), ['\alexs\yii2tubularload\TraitTubularload', 'tubularloadFilter']);
        }
        if (!empty($data)) {
            foreach ($data as $i=>$_data) {
                /** @var ActiveRecord $model */
                $model = new $model_name;
                $model->load($_data, '');
                static::tubularloadAttachBehaviors($model, $i, $behaviors);
                $models[] = $model;
            }
        }
        return $models;
    }

    /**
     * Loading models when we update them (they are exists)
     *
     * @param string $model_name
     * for example: 'app\models\City'
     * @param string|array|null $related_model_condition
     * for example: ['country_id'=>$country_id]
     * @param array $behaviors behaviors which have dynamic attributes
     * @param bool $throw_exception
     * @return ActiveRecord[]
     * @throws \InvalidArgumentException
     */
    public static function tubularloadUpdate($model_name, $related_model_condition = null, array $behaviors = [], $throw_exception = false) {
        $models = [];
        $data = static::tubularloadGetData($model_name);
        $pk_attr = static::$tubularload_pk_attr;
        // remove deleted models
        $ids = array_column($data, $pk_attr);
        /** @var ActiveRecord $model_name */
        if ($related_model_condition) {
            $model_name::deleteAll(['AND', $related_model_condition, ['NOT IN', $pk_attr, $ids]]);
        } else {
            $model_name::deleteAll(['NOT IN', $pk_attr, $ids]);
        }
        if (!empty($data)) {
            foreach ($data as $i=>$_data) {
                /** @var ActiveRecord $model */
                if (!empty($_data[$pk_attr])) {
                    if (!$model = $model_name::findOne($_data['id'])) {
                        if ($throw_exception) {
                            throw new \InvalidArgumentException;
                        }
                        // just skip the model
                        continue;
                    }
                    $model->load($_data, '');
                    static::tubularloadAttachBehaviors($model, $i, $behaviors);
                    $models[] = $model;
                } else {
                    if (static::$tubularload_filter_values) {
                        $_data = array_filter($_data, ['\alexs\yii2tubularload\TraitTubularload', 'tubularloadFilter']);
                    }
                    if (!empty($_data)) {
                        $model = new $model_name;
                        $model->load($_data, '');
                        static::tubularloadAttachBehaviors($model, $i, $behaviors);
                        $models[] = $model;
                    }
                }
            }
        }
        return $models;
    }

    /**
     * For example:
     * City::tubularloadSave($cities, 'country', $country)
     * City model should have a link
     * public function getCountry() {
     *     return $this->hasOne(Country::class, ['id'=>'country_id']);
     * }
     *
     * @param ActiveRecord[] $models
     * @param string|null $link_name
     * @param ActiveRecord|null $link_model
     */
    public static function tubularloadSave(array $models, $link_name = null, $link_model = null) {
        if (!empty($models)) {
            foreach ($models as $model) {
                if ($link_name && $link_model) {
                    $model->link($link_name, $link_model);
                }
                $model->save(false);
            }
        }
    }

    /**
     * @param string $model_name
     * for example: 'app\models\City'
     * @return array
     */
    public static function tubularloadGetData($model_name) {
        $basename = StringHelper::basename($model_name);
        return (array) \Yii::$app->request->post($basename);
    }

    /**
     * The callback for the array_filter method
     *
     * @param mixed $var
     * @return bool
     */
    public static function tubularloadFilter($var) {
        return !in_array($var, static::$tubularload_filtered_values, true);
    }

    /**
     * Attach behaviors which have dynamic attribute names
     *
     * @param ActiveRecord $model
     * @param int $i an index of the model in the post array
     * @param array $behaviors behaviors which have dynamic attributes
     * For example [
     * [
     *     [
     *         'class'=>Fileable::class,
     *         'upload_dir'=>City::getUploadDir(),
     *     ],
     *     ['attribute'=>'file1'],
     *     [...'Fileable1'],
     * ],
     * [
     *     [
     *         'class'=>Fileable::class,
     *         'upload_dir'=>City::getUploadDir(),
     *     ],
     *     ['attribute'=>'file2'],
     * ],
     * ]
     */
    protected static function tubularloadAttachBehaviors(ActiveRecord $model, $i, array $behaviors = []) {
        if (!empty($behaviors)) {
            foreach ($behaviors as $_i=>$_behavior) {
                list($behavior, $attributes) = $_behavior;
                $behavior_name = isset($_behavior[2]) ? $_behavior[2] : $_i;
                foreach ($attributes as $attribute_name=>$attribute) {
                    $behavior[$attribute_name] = "[$i]$attribute";
                }
                $model->attachBehavior($behavior_name, $behavior);
            }
        }
    }
}